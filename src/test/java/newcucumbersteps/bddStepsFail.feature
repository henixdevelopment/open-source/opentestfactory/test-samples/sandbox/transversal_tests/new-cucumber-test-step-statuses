Feature: Skipped Step Handling

  Scenario: Test Skipped Step
    Given I run a step
    When I run a failed step
    Then My step is skipped
    And My step is skipped